# (WIP) docker-compose-lemp-stack

## readme

This repository will get updated in near future to have a much better default config for php and nginx. For now nginx already got updated to strip error page signature/headers.

## https branch

This setup assumes you are not being fronted by e.g. cloudflare and have your own https certs provided by some service.

docker/docker-compose:
- If you don't have docker and docker-compose installed yet and you're on ubuntu 18+: `wget https://p.miosa.me/miosame/docker-and-compose-install-script/raw/branch/master/install.sh && chmod +x install.sh && ./install.sh`

https/http:
- If you **are** fronted by cloudflare and want to run this in http only, check out the `no-https` branch on this repo

mysql:
- If you need mysql: Fill out things marked as `XXX` in `docker-compose.yml`
- If you don't need mysql, remove lines 14-24 in `docker-compose.yml`

installation:
1. Put your https pem files into `nginx_keys` folder: `key.pem` and `cert.pem` (filename sensitive)
2. If you need mysql: Fill out things marked as `XXX` in `docker-compose.yml`
3. Now in the folder the `docker-compose.yml` is, execute: `docker-compose build && docker-compose up -d`
4. Your documentroot is now the `html` folder, anything you put there will be automatically passed through to the container and available immediately